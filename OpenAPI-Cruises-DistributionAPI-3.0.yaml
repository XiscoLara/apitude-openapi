openapi: 3.0.1
info:
  title: CRUISES B2B2C
  description: B2B2C Cruises api platform
  contact:
    email: B2B2C-Team@hotelbeds365.onmicrosoft.com
  license:
    name: B2B2C Creed
    url: http://www.hotelbeds.com
  version: "1.0.3"
servers:
- url: https://api.hotelbeds.com
  description: Generated server url
paths:
  /cruisesdistributionservice/1.0/availability/search:
    get:
      tags:
      - Cruises availability
      summary: Search for cruises availability
      description: Return availability result list
      operationId: searchAvailability
      parameters:
      - name: metadata
        in: query
        description: Get metadata flag
        required: false
        schema:
          type: boolean
      - name: searchDto
        in: query
        required: true
        schema:
          $ref: '#/components/schemas/SearchAvailabilityRequestDTO'
      - name: Api-key
        in: header
        description: Api-key
        required: true
        schema:
          type: string        
      responses:
        "500":
          description: Failure
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorDTO'
        "200":
          description: Success
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ResponseDTOSearchAvailabilityResponseDTO'
        "204":
          description: No Content
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorDTO'
        "401":
          description: Unauthorized
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorDTO'
        "404":
          description: Not Found
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorDTO'
        "403":
          description: Forbidden
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorDTO'
  /cruisesdistributionservice/1.0/availability/cabin-grades:
    post:
      tags:
      - Cabin grades availability
      summary: Cabin grades availability
      description: Cabin grades availability creates a new session and return all
        available cabin grades for each rate
      operationId: cabinGrades
      parameters:
      - name: language
        in: header
        required: true
        schema:
          type: string
          default: en
      - name: Api-key
        in: header
        description: Api-key
        required: true
        schema:
          type: string        
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CabinGradesRequest'
        required: true
      responses:
        "500":
          description: Failure
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorDTO'
        "200":
          description: Success
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ResponseDTOCabinGradesResponse'
        "204":
          description: No Content
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorDTO'
        "401":
          description: Unauthorized
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorDTO'
        "404":
          description: Not Found
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorDTO'
        "403":
          description: Forbidden
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorDTO'
components:
  schemas:
    SearchAvailabilityRequestDTO:
      required:
        - month
        - year
      type: object
      properties:
        year:
          maximum: 3000
          minimum: 2020
          type: integer
          description: Year for travel
          format: int32
        month:
          maximum: 12
          minimum: 1
          type: integer
          description: Month for travel
          format: int32
        lineId:
          type: integer
          description: Cruise line identifier
          format: int32
        areaId:
          type: integer
          description: Area identifier
          format: int32
        nights:
          type: integer
          description: How many nights you want cruises to be for
          format: int32
        shipId:
          type: integer
          description: Ships
          format: int32
      description: GET .../availability/search request object.
    ErrorDTO:
      type: object
      properties:
        dateTime:
          type: string
          description: Time and date the error was thrown
          format: date-time
        code:
          type: string
          description: Error code
        message:
          type: string
          description: Error message
        description:
          type: string
          description: Error description
        serviceName:
          type: string
          description: The name of the service that threw this error
        isBadRequest:
          type: boolean
          description: The error is caused by a bad request from client, it's not
            due to an internal service
        traceId:
          type: string
          description: The trace ID
        fieldErrors:
          type: array
          description: List of errors caused by field validations
          items:
            $ref: '#/components/schemas/FieldErrorDTO'
        nestedError:
          $ref: '#/components/schemas/ErrorDTO'
    FieldErrorDTO:
      type: object
      properties:
        code:
          type: string
          description: The field error code
        field:
          type: string
          description: The name of the field with the error
        message:
          type: string
          description: The field error message
      description: List of errors caused by field validations
    CabinPriceDTO:
      type: object
      properties:
        cabinType:
          type: string
          description: The cabin type
        amount:
          type: number
          description: The price amount
      description: Cabin price info
    CruiseDTO:
      type: object
      properties:
        codeToCruiseId:
          type: integer
          format: int32
        cruiseId:
          type: integer
          format: int32
        name:
          type: string
        engine:
          type: string
        startDate:
          type: string
          format: date
        endDate:
          type: string
          format: date
        nights:
          type: integer
          format: int32
        sailDate:
          type: string
          format: date
        sailNights:
          type: integer
          format: int32
        returnDate:
          type: string
          format: date
        groupIds:
          type: string
        priority:
          type: integer
          format: int32
        privateNotes:
          type: string
        resultWeight:
          type: integer
          format: int32
        roundTrip:
          type: boolean
        seaDays:
          type: integer
          format: int32
        senior:
          type: boolean
        soldOut:
          type: integer
          format: int32
        special:
          type: boolean
        specialsGroup:
          type: string
        stopLive:
          type: boolean
        voyageCode:
          type: string
        lineId:
          type: integer
          description: Line identifier
          format: int32
        ports:
          type: array
          items:
            $ref: '#/components/schemas/PortDTO'
        areas:
          type: array
          items:
            type: integer
            format: int32
        shipId:
          type: integer
          description: Ship identifier
          format: int32
        localPricing:
          type: string
        marketId:
          type: integer
          format: int32
        nettPrice:
          type: number
        price:
          type: number
        currency:
          type: string
        prices:
          type: array
          items:
            $ref: '#/components/schemas/CabinPriceDTO'
        taxes:
          type: number
      description: Cruise info
    LineDTO:
      type: object
      properties:
        id:
          type: integer
          description: Identififier
          format: int32
        code:
          type: string
          description: Code
        engine:
          type: string
          description: Engine
        logoUrl:
          type: string
          description: Logo url
        name:
          type: string
          description: Name
        niceUrl:
          type: string
          description: Nice image url
      description: Line info
    MetadataDTO:
      type: object
      properties:
        ships:
          type: array
          description: Ships
          items:
            $ref: '#/components/schemas/ShipDTO'
        lines:
          type: array
          description: Lines
          items:
            $ref: '#/components/schemas/LineDTO'
      description: Metadata info
    PortDTO:
      type: object
      properties:
        id:
          type: integer
          description: Port identifier
          format: int32
        name:
          type: string
          description: Port name
      description: Port info
    ResponseDTOSearchAvailabilityResponseDTO:
      type: object
      properties:
        response:
          $ref: '#/components/schemas/SearchAvailabilityResponseDTO'
        elapsedTime:
          type: integer
          format: int64
    SearchAvailabilityResponseDTO:
      type: object
      properties:
        cruises:
          type: array
          description: The cruises info.
          items:
            $ref: '#/components/schemas/CruiseDTO'
        metadata:
          $ref: '#/components/schemas/MetadataDTO'
      description: GET .../availability/search response object.
    ShipDTO:
      type: object
      properties:
        id:
          type: integer
          description: Identififier
          format: int32
        code:
          type: string
          description: Code
        name:
          type: string
          description: Name
        imageCaption:
          type: string
          description: Image caption
        imageUrl:
          type: string
          description: Image Url
        smallImageUrl:
          type: string
          description: Small Image Url
        niceUrl:
          type: string
          description: Nice Image Url
        rating:
          type: integer
          description: Ship rating
          format: int32
      description: Ship info
    CabinGradesRequest:
      required:
      - codeToCruiseId
      type: object
      properties:
        codeToCruiseId:
          type: integer
          description: Code to cruise identifier
          format: int32
      description: POST .../availability/cabin-grades/ request object.
    CabinGradesResponse:
      type: object
      properties:
        grades:
          type: array
          description: Grade list
          items:
            $ref: '#/components/schemas/Grade'
        itinerary:
          type: array
          description: Itinerary list
          items:
            $ref: '#/components/schemas/Itinerary'
        resultNo:
          type: string
          description: The result number given by the simpleSearch call
      description: POST .../availability/cabin-grades/ RESPONSE object.
    CabinTypeDTO:
      type: object
      properties:
        id:
          type: integer
          description: Identifier
          format: int32
        name:
          type: string
          description: Identifier
        position:
          type: string
          description: Cabin position
        code:
          type: string
          description: Cabin code
        extra:
          type: string
          description: Cabin extra
        deckId:
          type: integer
          description: Deck identifier
          format: int32
        description:
          type: string
          description: Description
        cabinCodes:
          type: array
          description: Cabin codes
          items:
            type: string
            description: Cabin codes
        cabinType:
          type: string
          description: Cabin type
        caption:
          type: string
          description: Caption
        colourCode:
          type: string
          description: Colour code
        isDefault:
          type: boolean
          description: Is default cabin type flag
        sortWeight:
          type: integer
          description: Sort weight
          format: int32
        validFrom:
          type: string
          description: Valid from
          format: date
        validTo:
          type: string
          description: Valid to
          format: date
        imageUrl:
          type: string
          description: Cabin image url
        smallImageUrl:
          type: string
          description: Cabin small image url
      description: Cabin Type info
    Grade:
      type: object
      properties:
        gradeId:
          type: string
          description: Grade Id
        gradeResultId:
          type: string
          description: Grade Result Id
        description:
          type: string
          description: Grade description
        price:
          type: number
          description: Price
        grossPrice:
          type: number
          description: Gross Price
        fees:
          type: number
          description: Fees
        taxes:
          type: number
          description: Taxes
        currency:
          type: string
          description: Currency
        categoryOrder:
          type: integer
          description: Category order
          format: int32
        doCabinsRequest:
          type: boolean
          description: Cabins request flag
        iata:
          type: string
          description: Iata code
        onBoardCredit:
          type: number
          description: On board credit
        rate:
          $ref: '#/components/schemas/RateDTO'
        cabinType:
          $ref: '#/components/schemas/CabinTypeDTO'
      description: Grade info
    Itinerary:
      type: object
      properties:
        item:
          type: array
          items:
            $ref: '#/components/schemas/ItineraryItemDTO'
        arriveDate:
          type: string
          format: date
        arriveInfo:
          type: string
        arriveTime:
          $ref: '#/components/schemas/LocalTime'
        day:
          type: integer
          format: int32
        departDate:
          type: string
          format: date
        departInfo:
          type: string
        departTime:
          $ref: '#/components/schemas/LocalTime'
        name:
          type: string
      description: Itinerary info
    ItineraryItemDTO:
      type: object
      properties:
        arriveDate:
          type: string
          format: date
        arriveInfo:
          type: string
        arriveTime:
          $ref: '#/components/schemas/LocalTime'
        description:
          type: string
        day:
          type: integer
          format: int32
        departDate:
          type: string
          format: date
        departInfo:
          type: string
        departTime:
          $ref: '#/components/schemas/LocalTime'
        name:
          type: string
        extraInfo:
          type: string
        latitude:
          type: string
        longitude:
          type: string
      description: Itinerary item info
    LocalTime:
      type: object
      properties:
        hour:
          type: integer
          format: int32
        minute:
          type: integer
          format: int32
        second:
          type: integer
          format: int32
        nano:
          type: integer
          format: int32
    RateDTO:
      type: object
      properties:
        code:
          type: string
          description: Fare code
        description:
          type: string
          description: Description
        name:
          type: string
          description: Fare name
        fareType:
          type: string
          description: Fare type
        nett:
          type: boolean
          description: Is net fare
        ageQualifier:
          type: string
          description: Age qualifier
        nonRefundable:
          type: boolean
          description: Is non refundable fare
        nonRefundableDeposit:
          type: boolean
          description: Is non refundable deposit
        invalidRePrice:
          type: boolean
          description: Is invalid reprice
        voyageCode:
          type: string
          description: Voyage code
        promoCode:
          type: string
          description: Promocode
        senior:
          type: boolean
          description: Senior rate flag
        military:
          type: boolean
          description: Military rate flag
        pastPassenger:
          type: boolean
        closedUser:
          type: boolean
      description: Rate info
    ResponseDTOCabinGradesResponse:
      type: object
      properties:
        response:
          $ref: '#/components/schemas/CabinGradesResponse'
        elapsedTime:
          type: integer
          format: int64